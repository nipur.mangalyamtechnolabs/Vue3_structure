import Vue from 'vue'
import { createRouter, createWebHistory } from "vue-router";

// Routes
import { canNavigate } from '@/libs/acl/routeProtection'
import { isUserLoggedIn, getUserData, getHomeRouteForLoggedInUser } from '@/auth/utils'
import auth from './routes/auth'



const router = createRouter({
  mode: 'history',
  base: '',
  history:createWebHistory(),
  scrollBehavior() {
    return { x: 0, y: 0 }
  },
  routes: [
    { path: '/', redirect: { name: 'login' } },
    ...auth,
   
    {
      path: '/:catchAll(.*)',
      redirect: 'error-404',
    },
  ],
})


router.beforeEach((to, _, next) => {

  const isLoggedIn = isUserLoggedIn()
  if (!canNavigate(to)) {
    // Redirect to login if not logged in
    if (!isLoggedIn) return next({ name: '/login' })

    // If logged in => not authorized
    return next({ name: 'not-authorized' })
  }

  const userData = getUserData();
  let companyName = '';
  if (userData && userData.company) {
    companyName = userData.company.name;
  } else {
    console.log(import.meta.env.APP_NAME, 'import.meta.env.APP_NAME asdasdds');
    if (import.meta.env.APP_NAME && import.meta.env.APP_NAME != undefined) {
      companyName = import.meta.env.APP_NAME;
    } else {
      companyName = 'Food Consumption';
    }
  }

  if (typeof to.meta.pageTitle == undefined && to.meta.pageTitle == "") {
    document.title = companyName;
  } else {
    document.title = to.meta.pageTitle + ' | ' + (companyName && companyName.length ? companyName : 'Food Consumption');
  }

    console.log('asdddddddddddddddddddd')
  // Redirect if logged in
  if (to.meta.redirectIfLoggedIn && isLoggedIn) {
    const userData = getUserData()
    next(getHomeRouteForLoggedInUser(userData ? 'admin' : null))
  }

  return next()
})

// ? For splash screen
// Remove afterEach hook if you are not using splash screen
router.afterEach(() => {

})

export default router
