export default [
    {
        path: '/error-404',
        name: 'error-404',
        component: () => import('@/views/error/Error404.vue'),
        meta: {
            layout: 'full',
            resource: 'Auth',
            action: 'read',
            pageTitle: '404',
        },
    },
    {
        path: '/login',
        name: 'login',
        component: () => import('@/views/admin/auth/Login.vue'),
        meta: {
            layout: 'full',
            resource: 'Auth',
            redirectIfLoggedIn: true,
            pageTitle: 'Login',
        },
    },
    {
        path: '/dashboard',
        name: 'dashboard',
        component: () => import('@/views/admin/auth/Dashboard.vue'),
        meta: {
            layout: 'full',
            resource: 'Auth',
            pageTitle: 'Dashboard',
        },
    },
    {
        path: '/forgot-password',
        name: 'forgot-password',
        component: () => import('@/views/admin/auth/ForgotPassword.vue'),
        meta: {
            layout: 'full',
            resource: 'Auth',
            redirectIfLoggedIn: false,
            pageTitle: 'Forgot Password',
        },
    },
    {
        path: '/set-password/:token',
        name: 'set-password',
        component: () => import('@/views/admin/auth/SetNewPassword.vue'),
        meta: {
            layout: 'full',
            resource: 'Auth',
            redirectIfLoggedIn: false,
            pageTitle: 'Set Password',
        },
    },
    {
        path: '/reset-password/:token',
        name: 'reset-password',
        component: () => import('@/views/admin/auth/ResetPassword.vue'),
        meta: {
            layout: 'full',
            resource: 'Auth',
            redirectIfLoggedIn: false,
            pageTitle: 'Reset Password Password',
        },
    },
    {
        path: '/unauthorized',
        name: 'not-authorized',
        component: () => import('@/views/admin/auth/NotAuthorized.vue'),
        meta: {
            layout: 'full',
            resource: 'Auth',
            pageTitle: 'Unauthorized',
        },
    },
]
