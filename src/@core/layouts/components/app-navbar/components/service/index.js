import { makereq } from '@/common/api';

export default {
  namespaced: true,
  state: {},
  getters: {},
  mutations: {},
  actions: {
        changePassword(ctx, data) {
            return new Promise((resolve, reject) => {
                makereq('/api/user/change-password', 'post', data)
                .then(response => resolve(response))
                .catch(error => reject(error))
            })
        },
    },
}