import { $themeBreakpoints } from '@themeConfig'
import store from "@/store";
export default {
  watch: {
    $route() {
      if (store.state.app.windowWidth < $themeBreakpoints.xl) {
        this.isVerticalMenuActive = false
      }
    },
  },
}
