// We haven't added icon's computed property because it makes this mixin coupled with UI
export const togglePasswordVisibility = {
  data() {
    return {
      passwordFieldType: 'password',
    }
  },
  methods: {
    togglePasswordVisibility() {
      console.log('asdadasdasdasdasdasd')
      this.passwordFieldType = this.passwordFieldType === 'password' ? 'text' : 'password'
    },
  },
}
export const toggleNewPasswordVisibility = {
  data() {
    return {
      newPasswordFieldType: 'password',
    }
  },
  methods: {
    toggleNewPasswordVisibility() {
      this.newPasswordFieldType = this.newPasswordFieldType === 'password' ? 'text' : 'password'
    },
  },
}
export const toggleConfirmPasswordVisibility = {
  data() {
    return {
      confirmPasswordFieldType: 'password',
    }
  },
  methods: {
    toggleConfirmPasswordVisibility() {
      this.confirmPasswordFieldType = this.confirmPasswordFieldType === 'password' ? 'text' : 'password'
    },
  },
}

export const _ = null
