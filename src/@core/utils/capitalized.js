import { onMounted, onUnmounted } from 'vue';

const capitalizeDirective = {
  mounted(el) {
    const capitalizeInput = () => {
        setTimeout(() => {
            el.value = el.value.charAt(0).toUpperCase() + el.value.slice(1);      
        }, 50);
      
    };

    el.addEventListener('input', capitalizeInput);

    // Cleanup function
    onUnmounted(() => {
      el.removeEventListener('input', capitalizeInput);
    });
  }
};

export default capitalizeDirective;