import axios from 'axios'
import Vue from 'vue';
import useJwt from '@/auth/jwt/useJwt'
import ToastificationContent from '@core/components/toastification/ToastificationContent.vue'
import router from "@/router";
import { useToast } from "vue-toastification";
const toast = useToast();
export function makereq(url, method, data, headers = '') {
  let accessToken = '';
  if (localStorage.getItem("userData")) {
    accessToken = localStorage.getItem("accessToken")
  }
  const headerconfig = accessToken ? {
    'Authorization': `Bearer ${accessToken}`
  } : {};
  var config = {
    method: method,
    url: import.meta.env.VITE_API_URL +  url,
    headers: headerconfig,
    data: data,
  };
  console.log('config');
  console.log(config);

  return new Promise(async (resolve, reject) => {
    await axios(config)
      .then((response) => resolve(response))
      .catch(function (error) {
        if (error.response) {
          if (error.response.status === 403) {
            window.location.href = '/unauthorized';
            return;
          }
          if (error.response.status === 401) {
            if (localStorage.getItem("userData")) {
              let title = 'Error';
              let icon = 'AlertCircleIcon';
              let text = error.response.data.message;
              let variant = 'danger';
              toast({
                component: ToastificationContent,
                props: {
                  title,
                  icon,
                  text,
                  variant,
                },
              });
              // localStorage.removeItem(useJwt.jwtConfig.storageTokenKeyName)
              // localStorage.removeItem(useJwt.jwtConfig.storageRefreshTokenKeyName)
              // localStorage.removeItem('userData');
              // router.push({ name: "login" });
            }
            return;
          }
          if (error.response.status === 500) {
            let title = 'Error';
            let icon = 'AlertCircleIcon';
            let text = 'Something Went Wrong! Try again';
            let variant = 'danger';
            toast({
              component: ToastificationContent,
              props: {
                title,
                icon,
                text,
                variant,
              },
            });
            // localStorage.removeItem(useJwt.jwtConfig.storageTokenKeyName)
            // localStorage.removeItem(useJwt.jwtConfig.storageRefreshTokenKeyName)
            // localStorage.removeItem('userData');
            // router.push({ name: "login" });
            // return;
          }
          reject(error);
        } else if (error.request) {
          reject(error);
        }
      });
  })
}