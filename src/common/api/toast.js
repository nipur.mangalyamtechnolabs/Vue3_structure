import { useToast } from "vue-toastification";
import ToastificationContent from '@core/components/toastification/ToastificationContent.vue'

export default function toast(type, message) {
  // Use toast
  const toast = useToast()
  const toaster = (ctx, callback) => {
    toast({
      component: ToastificationContent,
      props: {
        title: 'Error fetching list',
        icon: 'AlertTriangleIcon',
        variant: 'danger',
      }
    });
  }

  return {
    toaster,
  }
}
