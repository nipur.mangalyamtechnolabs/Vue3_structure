import { makereq } from "./utils";

const companyFactory = {
    list: async () => {
        try {
            const response = await makereq('/user/listing', 'get', '');
            return response.data;
        }
        catch (error) {
            toaster('error', error.message);
        }
    },
}

export default companyFactory;
