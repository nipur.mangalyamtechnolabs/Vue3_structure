import { createApp,h } from 'vue'
import App from './App.vue'
import router from './router'
import Toast from "vue-toastification";
import "vue-toastification/dist/index.css";
import { abilitiesPlugin } from '@casl/vue';
import { Ability } from '@casl/ability';
import FeatherIcon from './@core/components/feather-icon/FeatherIcon.vue';

import IconHome from "@/components/icons/IconHome.vue"
import IconUsers from "@/components/icons/IconUsers.vue"
import IconCircle from "@/components/icons/IconCircle.vue"
import IconSettings from "@/components/icons/IconSettings.vue"
import IconProfile from "@/components/icons/IconProfile.vue"
import IconChangePassword from "@/components/icons/IconChangePassword.vue"
import IconSignOut from "@/components/icons/IconSignOut.vue"
import IconEye from "@/components/icons/IconEye.vue"
import IconPencil from "@/components/icons/IconPencil.vue"
import IconEmailSend from "@/components/icons/IconEmailSend.vue"
import IconSpecialPassword from "@/components/icons/IconSpecialPassword.vue"
import IconFilter from "@/components/icons/IconFilter.vue"
import IconAction from "@/components/icons/IconAction.vue"
import IconPlusActive from "@/components/icons/IconPlusActive.vue"
import IconSearchLine from "@/components/icons/IconSearchLine.vue"
import IconBack from "@/components/icons/IconBack.vue"
import IconSearchMute from "@/components/icons/IconSearchMute.vue"
import IconRoles from "@/components/icons/IconRoles.vue"
import IconTrash from "@/components/icons/IconTrash.vue"
import IconTrashImage from "@/components/icons/IconTrashImage.vue"
import IconHistory from "@/components/icons/IconHistory.vue"
import IconWarning from "@/components/icons/IconWarning.vue"
import IconUserProfile from "@/components/icons/IconUserProfile.vue"
import IconBackHome from "@/components/icons/IconBackHome.vue"
import IconOtpResend from "@/components/icons/IconOtpResend.vue"
import IconOtpSend from "@/components/icons/IconOtpSend.vue"
import IconKey from "@/components/icons/IconKey.vue"
import { DatePicker } from 'v-calendar';
import './plugins/vee-validation-config';


const ability = new Ability()
const app  = createApp({
  render: ()=>h(App)
}).use(abilitiesPlugin, ability, {
  useGlobalProperties: true
}).use(router);


app.component(FeatherIcon.name, FeatherIcon);
app.component('VDatePicker', DatePicker);
app.component('IconHome', IconHome)
app.component('IconUsers', IconUsers)
app.component('IconCircle', IconCircle)
app.component('IconSettings', IconSettings)
app.component('IconProfile', IconProfile)
app.component('IconChangePassword', IconChangePassword)
app.component('IconSignOut', IconSignOut)
app.component('IconEye', IconEye)
app.component('IconPencil', IconPencil)
app.component('IconEmailSend', IconEmailSend)
app.component('IconFilter', IconFilter)
app.component('IconSpecialPassword', IconSpecialPassword)
app.component('IconAction', IconAction)
app.component('IconPlusActive', IconPlusActive)
app.component('IconSearchLine', IconSearchLine)
app.component('IconBack', IconBack)
app.component('IconSearchMute', IconSearchMute)
app.component('IconRoles', IconRoles)
app.component('icon-trash', IconTrash)
app.component('IconTrashImage', IconTrashImage)
app.component('IconTrash', IconTrash);
app.component('IconHistory', IconHistory)
app.component('IconWarning', IconWarning)
app.component('IconUserProfile', IconUserProfile)
app.component('IconBackHome', IconBackHome)
app.component('IconOtpResend', IconOtpResend)
app.component('IconOtpSend', IconOtpSend)
app.component('IconKey', IconKey)


app.component('TextInput', TextInput)
app.component('CheckboxInput', CheckboxInput)
app.component('SelectInput', SelectInput)
app.component('SelectTagInput', SelectTagInput)
app.component('ContactInput', ContactInput)
app.component('PasswordInput', PasswordInput)

app.directive('capitalize', capitalizeDirective);
const options = {
  timeout: 2000
};

import "./scss/core.scss";
import "./scss/main-scss/style.scss";
import TextInput from './components/form-fields/TextInput.vue';
import CheckboxInput from './components/form-fields/CheckboxInput.vue';
import SelectInput from './components/form-fields/SelectInput.vue';
import ContactInput from './components/form-fields/ContactInput.vue';
import PasswordInput from './components/form-fields/PasswordInput.vue';
import SelectTagInput from './components/form-fields/SelectTagInput.vue';
import capitalizeDirective from './@core/utils/capitalized';
app.use(Toast, options);
app.mount('#appMain')
