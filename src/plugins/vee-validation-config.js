import { configure } from 'vee-validate';
import * as yup from 'yup';

// Configure VeeValidate globally
configure({
  generateMessage: ctx => {
    const messages = {
      email: `Please enter valid Email ID`
    };

    return messages[ctx.rule.name] || `${ctx.field} is invalid`;
  },
  validateOnBlur: true,
  validateOnChange: true,
  validateOnInput: true,
  validateOnModelUpdate: true
});