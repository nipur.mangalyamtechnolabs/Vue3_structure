import { canNavigate } from '@/libs/acl/routeProtection'
import { isUserLoggedIn, getUserData, getHomeRouteForLoggedInUser } from '@/auth/utils'
import auth from './routes/auth'
import dashboard from './routes/dashboard'
import users from './routes/users'
import roles from './routes/roles'
import company from './routes/company'
import allUserList from './routes/all_users'
import IngredientsCategory from './routes/ingredients_category'
import FoodItemCategory from './routes/fooditem_category'
import { createRouter, createWebHistory } from "vue-router";



const router = createRouter({
  mode: 'history',
  base: '',
  history:createWebHistory(),
  scrollBehavior() {
    return { x: 0, y: 0 }
  },
  routes: [
    { path: '/', redirect: { name: 'dashboard' } },
    ...auth,
    ...dashboard,
    ...users,
    ...roles,
    ...company,
    ...allUserList,
    ...IngredientsCategory,
    ...FoodItemCategory,
    {
      path: '/:catchAll(.*)',
      redirect: 'error-404',
    },
  ],
})
// Vue.use(VueRouter)

// const router = new VueRouter({
//   mode: 'history',
//   base: process.env.BASE_URL,
//   scrollBehavior() {
//     return { x: 0, y: 0 }
//   },
//   routes: [
//     { path: '/', redirect: { name: 'dashboard' } },
//     ...auth,
//     ...dashboard,
//     ...users,
//     ...roles,
//     ...allUserList,
//     {
//       path: '*',
//       redirect: 'error-404',
//     },
//   ],
// })
// Vue.use(VueRouter)

// const router = new VueRouter({
//   mode: 'history',
//   base: process.env.BASE_URL,
//   scrollBehavior() {
//     return { x: 0, y: 0 }
//   },
//   routes: [
//     { path: '/', redirect: { name: 'dashboard' } },
//     ...auth,
//     ...dashboard,
//     ...users,
//     ...roles,
//     ...allUserList,
//     {
//       path: '*',
//       redirect: 'error-404',
//     },
//   ],
// })


router.beforeEach((to, _, next) => {
console.log('asdasdasdasdasdasdasd')
  const isLoggedIn = isUserLoggedIn()
  console.log(canNavigate(to), 'asdasdasdasdasdasdasdasd');
  if (!canNavigate(to)) {
    // Redirect to login if not logged in
    if (!isLoggedIn) return next({ name: 'login' })

    // If logged in => not authorized
    return next({ name: 'not-authorized' })
  }

  const userData = getUserData();
  let companyName = '';
  if (userData && userData.company) {
    companyName = userData.company.name;
  } else {
    console.log(import.meta.env.APP_NAME, 'import.meta.env.APP_NAME asdasdasdasdasdasdasd');
    if (import.meta.env.APP_NAME && import.meta.env.APP_NAME != undefined) {
      companyName = import.meta.env.APP_NAME;
    } else {
      companyName = '';
    }
  }

  if (typeof to.meta.pageTitle == undefined && to.meta.pageTitle == "") {
    document.title = companyName;
  } else {
    document.title = to.meta.pageTitle + ' | ' + (companyName && companyName.length ? companyName : 'Food Consumption');
  }

  // Redirect if logged in
  if (to.meta.redirectIfLoggedIn && isLoggedIn) {
    const userData = getUserData()
    next(getHomeRouteForLoggedInUser(userData ? 'admin' : null))
  }

  return next()
})

// ? For splash screen
// Remove afterEach hook if you are not using splash screen
router.afterEach(() => {

})

export default router;
