export default [
  {
    path: '/all-user',
    name: 'all-users-list',
    component: () => import('@/views/admin/all-users/Index.vue'),
    meta: {
      navActiveLink: 'all-users-list',
      resource: 'all_users_view',
      action: 'read',
      pageTitle: 'All Users',
      back: 'dashboard',
      type: 'list',
      navTitle: 'Your All<p class="text-red"> Users</p>',
      breadcrumb: [
        {
          text: 'Dashboard',
          route: 'dashboard'
        },
        {
          text: 'All Users',
          active: true
        }
      ],
     
    },
  },
  {
    path: '/all-user/view/:id',
    name: 'all-user-view',
    component: () => import('@/views/admin/all-users/Create.vue'),
    meta: {
      navActiveLink: 'all-users-list',
      resource: 'all_users_view',
      action: 'read',
      navTitle: 'View <p class="text-red"> User</p>',
      pageTitle: 'View User',
      back: 'all-users-list',
      type: 'view',
      breadcrumb: [
        {
          text: 'Dashboard',
          route: 'dashboard'
        },
        {
          text: 'All Users',
          route: 'all-users-list'
        },
        {
          text: 'View User',
          active: true,
        }
      ],
    },
  },
  {
    path: '/all-user/edit/:id',
    name: 'all-user-edit',
    component: () => import('@/views/admin/all-users/Create.vue'),
    meta: {
      navActiveLink: 'all-users-list',
      resource: 'all_users_view',
      action: 'read',
      navTitle: 'Edit <p class="text-red"> User</p>',
      pageTitle: 'Edit User',
      back: 'all-users-list',
      type: 'edit',
      breadcrumb: [
        {
          text: 'Dashboard',
          route: 'dashboard'
        },
        {
          text: 'All Users',
          route: 'all-users-list'
        },
        {
          text: 'Edit User',
          active: true,
        }
      ],
    },
  },
]