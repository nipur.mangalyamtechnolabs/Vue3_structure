export default [
  {
    path: '/company',
    name: 'company-list',
    component: () => import('@/views/admin/company/Index.vue'),
    meta: {
      navActiveLink: 'company-list',
      pageTitle: 'Company',
      back: 'dashboard',
      type: 'list',
      navTitle: 'Your <p class="text-red"> Companies</p>',
      breadcrumb: [
        {
          text: 'Dashboard',
          route: 'dashboard'
        },
        {
          text: 'Company',
          active: true
        }
      ],
      resource: 'company_view',
      action: 'read',
    },
  },
  {
    path: '/company/create',
    name: 'company-create',
    component: () => import('@/views/admin/company/Create.vue'),
    meta: {
      navActiveLink: 'company-list',
      resource: 'company_add',
      action: 'read',
      pageTitle: 'Add Company',
      navTitle: 'Create <p class="text-red"> Company</p>',
      back: 'company-list',
      type: 'create',
      breadcrumb: [
        {
          text: 'Dashboard',
          route: 'dashboard'
        },
        {
          text: 'Company',
          route: 'company-list'
        },
        {
          text: 'Add Company',
          active: true,
        }
      ],
    },
  },
  {
    path: '/company/view/:id',
    name: 'company-view',
    component: () => import('@/views/admin/company/Create.vue'),
    meta: {
      navActiveLink: 'company-list',
      resource: 'company_view',
      action: 'read',
      navTitle: 'View <p class="text-red"> Company</p>',
      pageTitle: 'View Company',
      back: 'company-list',
      type: 'view',
      breadcrumb: [
        {
          text: 'Dashboard',
          route: 'dashboard'
        },
        {
          text: 'Company',
          route: 'company-list'
        },
        {
          text: 'View Company',
          active: true,
        }
      ],
    },
  },
  {
    path: '/company/edit/:id',
    name: 'company-edit',
    component: () => import('@/views/admin/company/Create.vue'),
    meta: {
      navActiveLink: 'company-list',
      resource: 'company_edit',
      action: 'read',
      navTitle: 'Edit <p class="text-red"> Company</p>',
      pageTitle: 'Edit Company',
      back: 'company-list',
      type: 'edit',
      breadcrumb: [
        {
          text: 'Dashboard',
          route: 'dashboard'
        },
        {
          text: 'Company',
          route: 'company-list'
        },
        {
          text: 'Edit Company',
          active: true,
        }
      ],
    },
  },
]
