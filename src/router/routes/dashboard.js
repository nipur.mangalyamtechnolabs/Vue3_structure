export default [
  {
    path: '/dashboard',
    name: 'dashboard',
    component: () => import('@/views/admin/dashboard/Dashboard.vue'),
    meta: {
      resource: 'Dashboard',
      action: 'read',
      pageTitle: 'Dashboard',
      breadcrumb: [
      ],
    },
  },
  {
    path: '/settings',
    name: 'settings',
    component: () => import('@/views/admin/settings/Settings.vue'),
    meta: {
      resource: 'settings_index',
      action: 'read',
      navTitle: 'My <p class="text-red">Settings</p>',
      pageTitle: 'Settings',
      back: 'dashboard',
      breadcrumb: [
        {
          text: 'Dashboard',
          route: 'dashboard'
        },
        {
          text: 'Settings',
          active: true,
        }
      ],
    },
  },
]
