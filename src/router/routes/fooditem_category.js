export default [
    {
      path: '/food-item-categories',
      name: 'food-item-category-list',
      component: () => import('@/views/admin/food-item-category/Index.vue'),
      meta: {
        navActiveLink: 'food-item-category-list',
        resource: 'fooditem_category_view',
        action: 'read',
        navTitle: 'Your <p class="text-red"> Food Item Categories</p>',
        pageTitle: 'FoodItem Categories',
        back: 'dashboard',
        type: 'list',
        breadcrumb: [
          {
            text: 'Dashboard',
            route: 'dashboard'
          },
          {
            text: 'Food Item Categories',
            active: true,
          }
        ],
      },
    },
    {
      path: '/food-item-category/create',
      name: 'food-item-category-create',
      component: () => import('@/views/admin/food-item-category/Create.vue'),
      meta: {
        navActiveLink: 'food-item-category-list',
        resource: 'fooditem_category_add',
        action: 'read',
        navTitle: 'Create <p class="text-red">Food Item Category</p>',
        pageTitle: 'Add Food Item Category',
        back: 'food-item-category-list',
        type: 'create',
        breadcrumb: [
          {
            text: 'Dashboard',
            route: 'dashboard'
          },
          {
            text: 'Food Item Categories',
            route: 'food-item-category-list'
          },
          {
            text: 'Add Food Item Category',
            active: true,
          }
        ],
      },
    },
    {
      path: '/food-item-category/view/:id',
      name: 'food-item-category-view',
      component: () => import('@/views/admin/food-item-category/Create.vue'),
      meta: {
        navActiveLink: 'food-item-category-list',
        resource: 'fooditem_category_view',
        action: 'read',
        navTitle: 'View <p class="text-red"> Ingredients Category</p>',
        pageTitle: 'View Ingredients Category',
        back: 'food-item-category-list',
        type: 'view',
        breadcrumb: [
          {
            text: 'Dashboard',
            route: 'dashboard'
          },
          {
            text: 'Food Item Categories',
            route: 'food-item-category-list'
          },
          {
            text: 'View Ingredients Category',
            active: true,
          }
        ],
      },
    },
    {
      path: '/food-item-category/edit/:id',
      name: 'food-item-category-edit',
      component: () => import('@/views/admin/food-item-category/Create.vue'),
      meta: {
        navActiveLink: 'food-item-category-list',
        resource: 'fooditem_category_edit',
        action: 'read',
        navTitle: 'Edit <p class="text-red">Food Item Category</p>',
        pageTitle: 'Edit User',
        back: 'food-item-category-list',
        type: 'edit',
        breadcrumb: [
          {
            text: 'Dashboard',
            route: 'dashboard'
          },
          {
            text: 'Food Item Categories',
            route: 'food-item-category-list'
          },
          {
            text: 'Edit Food Item Category',
            active: true,
          }
        ],
      },
    },
  ]
  