export default [
    {
      path: '/ingredients-categories',
      name: 'ingredients-category-list',
      component: () => import('@/views/admin/ingredients-category/Index.vue'),
      meta: {
        navActiveLink: 'ingredients-category-list',
        resource: 'ingredients_category_view',
        action: 'read',
        navTitle: 'Your <p class="text-red">Ingredients Categories</p>',
        pageTitle: 'Ingredients Categories',
        back: 'dashboard',
        type: 'list',
        breadcrumb: [
          {
            text: 'Dashboard',
            route: 'dashboard'
          },
          {
            text: 'Ingredients Categories',
            active: true,
          }
        ],
      },
    },
    {
      path: '/ingredients-category/create',
      name: 'ingredients-category-create',
      component: () => import('@/views/admin/ingredients-category/Create.vue'),
      meta: {
        navActiveLink: 'ingredients-category-list',
        resource: 'ingredients_category_add',
        action: 'read',
        navTitle: 'Create <p class="text-red"> Ingredients Category</p>',
        pageTitle: 'Add Ingredients Category',
        back: 'ingredients-category-list',
        type: 'create',
        breadcrumb: [
          {
            text: 'Dashboard',
            route: 'dashboard'
          },
          {
            text: 'Ingredients Categories',
            route: 'ingredients-category-list'
          },
          {
            text: 'Add Ingredients Category',
            active: true,
          }
        ],
      },
    },
    {
      path: '/ingredients-category/view/:id',
      name: 'ingredients-category-view',
      component: () => import('@/views/admin/ingredients-category/Create.vue'),
      meta: {
        navActiveLink: 'ingredients-category-list',
        resource: 'ingredients_category_view',
        action: 'read',
        navTitle: 'View <p class="text-red"> Ingredients Category</p>',
        pageTitle: 'View Ingredients Category',
        back: 'ingredients-category-list',
        type: 'view',
        breadcrumb: [
          {
            text: 'Dashboard',
            route: 'dashboard'
          },
          {
            text: 'Ingredients Categories',
            route: 'ingredients-category-list'
          },
          {
            text: 'View Ingredients Category',
            active: true,
          }
        ],
      },
    },
    {
      path: '/ingredients-category/edit/:id',
      name: 'ingredients-category-edit',
      component: () => import('@/views/admin/ingredients-category/Create.vue'),
      meta: {
        navActiveLink: 'ingredients-category-list',
        resource: 'ingredients_category_edit',
        action: 'read',
        navTitle: 'Edit <p class="text-red"> Ingredients Category</p>',
        pageTitle: 'Edit User',
        back: 'ingredients-category-list',
        type: 'edit',
        breadcrumb: [
          {
            text: 'Dashboard',
            route: 'dashboard'
          },
          {
            text: 'Ingredients Categories',
            route: 'ingredients-category-list'
          },
          {
            text: 'Edit Ingredients Category',
            active: true,
          }
        ],
      },
    },
  ]
  