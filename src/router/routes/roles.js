export default [
  {
    path: '/roles',
    name: 'role-list',
    component: () => import('@/views/admin/roles/Index.vue'),
    meta: {
      navActiveLink: 'role-list',
      resource: 'roles_view',
      action: 'read',
      pageTitle: 'Role',
      navTitle: 'Your <p class="text-red"> Roles</p>',
      back: 'dashboard',
      type: 'list',
      breadcrumb: [
        {
          text: 'Dashboard',
          route: 'dashboard'
        },
        {
          text: 'Role',
          active: true,
        }
      ],
    },
  },
  {
    path: '/role/create',
    name: 'role-create',
    component: () => import('@/views/admin/roles/Create.vue'),
    meta: {
      navActiveLink: 'role-list',
      resource: 'roles_add',
      action: 'read',
      pageTitle: 'Add Role',
      navTitle: 'Create <p class="text-red"> Role</p>',
      back: 'role-list',
      type: 'create',
      breadcrumb: [
        {
          text: 'Dashboard',
          route: 'dashboard'
        },
        {
          text: 'Role',
          route: 'role-list'
        },
        {
          text: 'Add Role',
          active: true,
        }
      ],
    },
  },
  {
    path: '/role/view/:id',
    name: 'role-view',
    component: () => import('@/views/admin/roles/Create.vue'),
    meta: {
      navActiveLink: 'role-list',
      resource: 'roles_view',
      action: 'read',
      pageTitle: 'View Role',
      navTitle: 'View <p class="text-red"> Role</p>',
      back: 'role-list',
      type: 'view',
      breadcrumb: [
        {
          text: 'Dashboard',
          route: 'dashboard'
        },
        {
          text: 'Role',
          route: 'role-list'
        },
        {
          text: 'View Role',
          active: true,
        }
      ],
    },
  },
  {
    path: '/role/edit/:id',
    name: 'role-edit',
    component: () => import('@/views/admin/roles/Create.vue'),
    meta: {
      navActiveLink: 'role-list',
      resource: 'roles_edit',
      action: 'read',
      pageTitle: 'Edit Role',
      navTitle: 'Edit <p class="text-red"> Role</p>',
      back: 'role-list',
      type: 'edit',
      breadcrumb: [
        {
          text: 'Dashboard',
          route: 'dashboard'
        },
        {
          text: 'Role',
          route: 'role-list'
        },
        {
          text: 'Edit Role',
          active: true,
        }
      ],
    },
  },
]
