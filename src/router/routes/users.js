export default [
  {
    path: '/user',
    name: 'user-list',
    component: () => import('@/views/admin/users/Index.vue'),
    meta: {
      navActiveLink: 'user-list',
      resource: 'users_view',
      action: 'read',
      navTitle: 'Your <p class="text-red"> Users</p>',
      pageTitle: 'User',
      back: 'dashboard',
      type: 'list',
      breadcrumb: [
        {
          text: 'Dashboard',
          route: 'dashboard'
        },
        {
          text: 'User',
          active: true,
        }
      ],
    },
  },
  {
    path: '/user/create',
    name: 'user-create',
    component: () => import('@/views/admin/users/Create.vue'),
    meta: {
      navActiveLink: 'user-list',
      resource: 'users_add',
      action: 'read',
      navTitle: 'Create <p class="text-red"> User</p>',
      pageTitle: 'Add User',
      back: 'user-list',
      type: 'create',
      breadcrumb: [
        {
          text: 'Dashboard',
          route: 'dashboard'
        },
        {
          text: 'User',
          route: 'user-list'
        },
        {
          text: 'Add User',
          active: true,
        }
      ],
    },
  },
  {
    path: '/user/view/:id',
    name: 'user-view',
    component: () => import('@/views/admin/users/Create.vue'),
    meta: {
      navActiveLink: 'user-list',
      resource: 'users_view',
      action: 'read',
      navTitle: 'View <p class="text-red"> User</p>',
      pageTitle: 'View User',
      back: 'user-list',
      type: 'view',
      breadcrumb: [
        {
          text: 'Dashboard',
          route: 'dashboard'
        },
        {
          text: 'User',
          route: 'user-list'
        },
        {
          text: 'View User',
          active: true,
        }
      ],
    },
  },
  {
    path: '/user/edit/:id',
    name: 'user-edit',
    component: () => import('@/views/admin/users/Create.vue'),
    meta: {
      navActiveLink: 'user-list',
      resource: 'users_edit',
      action: 'read',
      navTitle: 'Edit <p class="text-red"> User</p>',
      pageTitle: 'Edit User',
      back: 'user-list',
      type: 'edit',
      breadcrumb: [
        {
          text: 'Dashboard',
          route: 'dashboard'
        },
        {
          text: 'User',
          route: 'user-list'
        },
        {
          text: 'Edit User',
          active: true,
        }
      ],
    },
  },
  {
    path: '/user/profile',
    name: 'user-profile',
    component: () => import('@/views/admin/users/MyProfile.vue'),
    meta: {
      resource: 'Auth',
      action: 'read',
      navTitle: 'My <p class="text-red">Profile</p>',
      pageTitle: 'My Profile',
      back: 'dashboard',
      type: 'view',
      breadcrumb: [
        {
          text: 'Dashboard',
          route: 'dashboard'
        },
        {
          text: 'Profile',
          active: true,
        }
      ],
    },
  },
]
