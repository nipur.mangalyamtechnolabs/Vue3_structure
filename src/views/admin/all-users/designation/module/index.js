import { ref, watch, computed } from 'vue'
import store from '@/store'
import { title } from '@core/utils/filter'
import { useToast } from "vue-toastification";
import ToastificationContent from '@core/components/toastification/ToastificationContent.vue'

export default function designationModule() {
  // Use toast
  const toast = useToast()

  const refDesignationListTable = ref(null)

  // Table Handlers
  const tableColumns = [
    { key: "sr_no"},
    { key: 'name', sortable: true },
    { key: 'actions' },
  ]
  const perPage = ref(10)
  const totalUsers = ref(0)
  const currentPage = ref(1)
  const perPageOptions = [10, 25, 50, 100]
  const searchQuery = ''
  const sortBy = ref('id')
  const isSortDirDesc = ref(true)

  const dataMeta = computed(() => {
    const localItemsCount = refDesignationListTable.value ? refDesignationListTable.value.localItems.length : 0
    return {
      from: perPage.value * (currentPage.value - 1) + (localItemsCount ? 1 : 0),
      to: perPage.value * (currentPage.value - 1) + localItemsCount,
      of: totalUsers.value,
    }
  })

  const refetchData = () => {
    refDesignationListTable.value.refresh()
  }

  watch([currentPage, perPage, searchQuery], () => {
    refetchData()
  })

  const list = (ctx, callback) => {
    store.dispatch('designation/list', {
      pagination: true,
      search: searchQuery.value,
      perPage: perPage.value,
      page: currentPage.value,
      sortBy: sortBy.value,
      sortDesc: isSortDirDesc.value ? 'asc' : 'desc',
    })
      .then(response => {
        const data = response.data.result;
        totalData.value = response.data.result.total;
      })
      .catch(() => {
        toast({
          component: ToastificationContent,
          props: {
            title: 'Error fetching designation list',
            icon: 'AlertTriangleIcon',
            variant: 'danger',
          },
        })
      })
  }

  return {
    list,
    tableColumns,
    perPage,
    currentPage,
    totalUsers,
    dataMeta,
    perPageOptions,
    searchQuery,
    sortBy,
    isSortDirDesc,
    refDesignationListTable,
    refetchData,
  }
}
