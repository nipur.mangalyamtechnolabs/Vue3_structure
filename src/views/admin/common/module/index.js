import { ref, watch, computed } from 'vue'
import store from '@/store'
import { title } from '@core/utils/filter'
import { useToast } from "vue-toastification";
import ToastificationContent from '@core/components/toastification/ToastificationContent.vue'

export default function commonModule(moduleName, url = '', data = {}, sortByField = 'id', sortDesc = true) {
  const appLoading = document.getElementById('loading-bg');
  if (appLoading) {
    appLoading.style.display = 'block'
  }
  // Use toast
  const module = moduleName

  const toast = useToast()

  const refListTable = ref(null)

  const perPage = ref(10)
  const totalData = ref(0)
  const perPageValue = ref(0)
  const allCount = ref(0)
  const moduleColumn = ref(0)
  const currentPage = ref(1)
  const perPageOptions = [10, 25, 50, 100, 'All']
  const searchQuery = ref('')
  let filterData = ref(data);
  const sortBy = ref(sortByField)
  const isSortDirDesc = ref(sortDesc)

  const dataMeta = computed(() => {
    const localItemsCount = refListTable.value ? refListTable.value.localItems.length : 0

    const perPageValueData = perPage.value;
    var data = {
      from: perPage.value * (currentPage.value - 1) + (localItemsCount ? 1 : 0),
      to: perPage.value * (currentPage.value - 1) + localItemsCount,
      of: totalData.value,
      all: allCount.value,
    }
    if (perPage.value == 'All') {
      perPageValue.value = localItemsCount;
      data.from = localItemsCount * (currentPage.value - 1) + (localItemsCount ? 1 : 0);
      data.to = localItemsCount* (currentPage.value - 1) + localItemsCount;
    } else {
      perPageValue.value = perPage.value
    }

    return data;
  })

  const refetchData = () => {
    refListTable.value.refresh()
  }

  watch([currentPage, perPage, searchQuery, filterData], () => {
    console.log(perPage, 'perPageperPageperPageperPage')
    refetchData()
  })

  const fetchTableData = (ctx, callback) => {
    var send_url = module + '/list'
    if (url != '') {
      send_url = url;
    }
    console.log('send_url');
    console.log(send_url);
    let allFilter = {
      pagination: true,
      search: searchQuery.value,
      page: currentPage.value,
      sortBy: sortBy.value,
      sortDesc: isSortDirDesc.value ? 'asc' : 'desc',
      data: filterData.value
    };
    if (perPage.value != "All") {
      allFilter.perPage = perPage.value;
    }
    store.dispatch(send_url, allFilter)
      .then(response => {
        let users;
        users = (response.data.hasOwnProperty('result')) ? response.data.result.data : response.data.data.data;
        totalData.value = (response.data.hasOwnProperty('result')) ? response.data.result.total : response.data.data.total;
        allCount.value = (response.data.hasOwnProperty('result')) ? response.data.result.total : response.data.data.total;
        moduleColumn.value = (response.data.hasOwnProperty('result')) ? response.data.result.column : response.data.data.column;
        callback(users);
        const appLoading = document.getElementById('loading-bg')
        if (appLoading) {
          appLoading.style.display = 'none';
        }
      })
      .catch((e) => {
        const appLoading = document.getElementById('loading-bg')
        if (appLoading) {
          appLoading.style.display = 'none';
        }
        toast({
          component: ToastificationContent,
          props: {
            title: 'Error fetching list',
            icon: 'AlertTriangleIcon',
            variant: 'danger',
          },
        })
      })
  }

  const resolveUserStatusVariant = status => {
    if (status === 1) return 'success'
    if (status === 2) return 'secondary'
    return 'success'
  }

  const resolveUserStatusText = status => {
    if (status === 1) return 'Active'
    if (status === 2) return 'Inactive'
    return 'Active'
  }

  return {
    fetchTableData,
    perPage,
    moduleColumn,
    currentPage,
    totalData,
    allCount,
    dataMeta,
    perPageOptions,
    perPageValue,
    filterData,
    searchQuery,
    sortBy,
    isSortDirDesc,
    refListTable,
    resolveUserStatusVariant,
    resolveUserStatusText,
    refetchData,
  }
}
