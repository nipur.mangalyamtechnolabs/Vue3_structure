
import { makereq } from '@/common/api';

export default {
  namespaced: true,
  state: {},
  getters: {},
  mutations: {},
  actions: {
    export(ctx, data) {
      return new Promise((resolve, reject) => {
        makereq('/api/company/export', 'post', data)
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    list: async (ctx, data) => {
      return new Promise((resolve, reject) => {
        makereq('/api/company', 'post', data)
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    edit(ctx, { id }) {
      return new Promise((resolve, reject) => {
        makereq(`/api/company/edit/${id}`, 'get')
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    save(ctx, data) {
      return new Promise((resolve, reject) => {
        makereq(`/api/company/save`, 'post', data)
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    reSend(ctx, data) {
      return new Promise((resolve, reject) => {
        makereq(`/api/send-password-email`, 'post', data)
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    countryCode() {
      return new Promise((resolve, reject) => {
        makereq(`/api/country-code`, 'get')
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    status(ctx, data) {
      return new Promise((resolve, reject) => {
        makereq('/api/company/status', 'post', data)
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    }
  },

}
