import { makereq } from '@/common/api';

export default {
  namespaced: true,
  state: {},
  getters: {},
  mutations: {},
  actions: {
    user(ctx, { id }) {
      return new Promise((resolve, reject) => {
        makereq(`/api/user/${id}`, 'get')
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
  },
}
