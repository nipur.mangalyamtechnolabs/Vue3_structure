import { makereq } from '@/common/api';

export default {
  namespaced: true,
  state: {},
  getters: {},
  mutations: {},
  actions: {
    list: async (ctx, data) => {
      return new Promise((resolve, reject) => {
        makereq('/api/food-item-category/list', 'post', data)
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    edit(ctx, { id }) {
      return new Promise((resolve, reject) => {
        makereq(`/api/food-item-category/view/${id}`, 'get')
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    save(ctx, data) {
      return new Promise((resolve, reject) => {
        makereq('/api/food-item-category/save', 'post', data)
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    export(ctx, data) {
      return new Promise((resolve, reject) => {
        makereq('/api/food-item-category/export', 'post', data)
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    status(ctx, data) {
      return new Promise((resolve, reject) => {
        makereq('/api/food-item-category/status', 'post', data)
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    delete(ctx, data) {
      return new Promise((resolve, reject) => {
        makereq('/api/food-item-category/delete', 'post', data)
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
  },
}
