import { makereq } from '@/common/api';

export default {
  namespaced: true,
  state: {},
  getters: {},
  mutations: {},
  actions: {
    list: async (ctx, data) => {
      return new Promise((resolve, reject) => {
        makereq('/api/ingredient-category/list', 'post', data)
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    edit(ctx, { id }) {
      return new Promise((resolve, reject) => {
        makereq(`/api/ingredient-category/view/${id}`, 'get')
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    save(ctx, data) {
      return new Promise((resolve, reject) => {
        makereq('/api/ingredient-category/save', 'post', data)
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    export(ctx, data) {
      return new Promise((resolve, reject) => {
        makereq('/api/ingredient-category/export', 'post', data)
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    status(ctx, data) {
      return new Promise((resolve, reject) => {
        makereq('/api/ingredient-category/status', 'post', data)
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    delete(ctx, data) {
      return new Promise((resolve, reject) => {
        makereq('/api/ingredient-category/delete', 'post', data)
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    }
  },
}
