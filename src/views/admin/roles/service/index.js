import { makereq } from '@/common/api';

export default {
  namespaced: true,
  state: {},
  getters: {},
  mutations: {},
  actions: {
    export(ctx, data) {
      return new Promise((resolve, reject) => {
        makereq('/api/roles/export', 'post', data)
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    list: async (ctx, data) => {
      return new Promise((resolve, reject) => {
        makereq('/api/roles', 'post', data)
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    edit(ctx, { id }) {
      return new Promise((resolve, reject) => {
        makereq(`/api/roles/edit/${id}`, 'get')
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    status(ctx, data) {
      return new Promise((resolve, reject) => {
        makereq('/api/roles/status', 'post', data)
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    save(ctx, data) {
      return new Promise((resolve, reject) => {
        makereq('/api/roles/save', 'post', data)
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    delete(ctx, data) {
      return new Promise((resolve, reject) => {
        makereq('/api/role/delete', 'post', data)
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    permissionList(ctx) {
      return new Promise((resolve, reject) => {
        makereq(`/api/roles/permissions`, 'get')
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    permissionByRole(ctx, { id }) {
      return new Promise((resolve, reject) => {
        makereq(`/api/roles/permissions/${id}`, 'get')
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
  },
}
