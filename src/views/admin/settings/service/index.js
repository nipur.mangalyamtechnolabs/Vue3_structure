import axios from 'axios'

export default {
  namespaced: true,
  state: {},
  getters: {},
  mutations: {},
  actions: {
    settings(ctx) {
      return new Promise((resolve, reject) => {
        axios
          .get(import.meta.env.VITE_API_URL + `/api/company/settings`)
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    save(ctx, data) {
      return new Promise((resolve, reject) => {
        axios
          .post(import.meta.env.VITE_API_URL + '/api/company/settings/save', data)
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    countryCode() {
      return new Promise((resolve, reject) => {
        axios
          .get(import.meta.env.VITE_API_URL + `/api/country-code`)
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    sosPasswordSet(ctx, data) {
      return new Promise((resolve, reject) => {
        axios
          .post(import.meta.env.VITE_API_URL + '/api/company/set-sos-password', data)
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
  },
}
