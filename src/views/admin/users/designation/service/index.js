import { makereq } from '@/common/api';

export default {
  namespaced: true,
  state: {},
  getters: {},
  mutations: {},
  actions: {
    list(ctx, data) {
      return new Promise((resolve, reject) => {
        makereq('/api/designations', 'post', data)
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    save(ctx, data) {
      return new Promise((resolve, reject) => {
        makereq('/api/designations/save', 'post', data)
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
  },
}
