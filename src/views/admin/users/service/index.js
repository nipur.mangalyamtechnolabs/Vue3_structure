import { makereq } from '@/common/api';

export default {
  namespaced: true,
  state: {},
  getters: {},
  mutations: {},
  actions: {
    export(ctx, data){
      return new Promise((resolve, reject) => {
        makereq('/api/user/export', 'post', data)
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    status(ctx, data){
      return new Promise((resolve, reject) => {
        makereq('/api/user/status', 'post', data)
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    locked(ctx, data){
      return new Promise((resolve, reject) => {
        makereq('/api/user/is-locked-update', 'post', data)
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    list: async (ctx, data) => {
      return new Promise((resolve, reject) => {
        makereq('/api/users', 'post', data)
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },

    edit(ctx, { id }) {
      return new Promise((resolve, reject) => {
        makereq(`/api/user/edit/${id}`, 'get')
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    reSend(ctx, data) {
      return new Promise((resolve, reject) => {
        makereq(`/api/send-password-email`, 'post', data)
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    save(ctx, data) {
      return new Promise((resolve, reject) => {
        makereq('/api/user/save', 'post', data)
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },

    login_history: async (ctx, data) => {
      return new Promise((resolve, reject) => {
        makereq('/api/user/login_history', 'post', data)
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },

    companyDetails() {
      return new Promise((resolve, reject) => {
        makereq('/api/user/company-details', 'get')
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    countryCode() {
      return new Promise((resolve, reject) => {
        makereq('/api/country-code', 'get')
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    userOptionList() {
      return new Promise((resolve, reject) => {
        makereq('/api/users-options', 'get')
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    companyList() {
      return new Promise((resolve, reject) => {
        makereq('/api/user/company-list', 'get')
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    companyDesignationList(ctx, { id }) {
      return new Promise((resolve, reject) => {
        makereq(`/api/user/desigation-list/${id}`, 'get')
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    permissionList(ctx) {
      return new Promise((resolve, reject) => {
        makereq(`/api/roles/permissions`, 'get')
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    permissionByRole(ctx, { id }) {
      return new Promise((resolve, reject) => {
        makereq(`/api/roles/permissions/${id}`, 'get')
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
  },
}
