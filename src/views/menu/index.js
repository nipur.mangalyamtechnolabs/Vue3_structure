/*

Array of object

Top level object can be:
1. Header
2. Group (Group can have navItems as children)
3. navItem

* Supported Options

/--- Header ---/

header

/--- nav Grp ---/

title
icon (if it's on top level)
tag
tagVariant
children

/--- nav Item ---/

icon (if it's on top level)
title
route: [route_obj/route_name] (I have to resolve name somehow from the route obj)
tag
tagVariant

*/

// Array of sections
export default [
    {
        title: 'Dashboard',
        iconComponent: 'icon-home',
        icon: '',
        iconImage: 'smart-home.svg',
        route: 'dashboard',
        action: 'read',
        resource: 'Auth',
    },
    {
        title: 'Ingredients Category',
        iconComponent: 'icon-key',
        icon: '',
        iconImage: '',
        route: 'ingredients-category-list',
        action: 'read',
        resource: 'ingredients_category_list',
    },
    {
        title: 'Food Item Category',
        iconComponent: 'icon-key',
        icon: '',
        iconImage: '',
        route: 'food-item-category-list',
        action: 'read',
        resource: 'fooditem_category_list',
    },
    {
        title: 'User',
        iconComponent: 'icon-users',
        icon: '',
        iconImage: 'users.svg',
        route: 'user-list',
        action: 'read',
        resource: 'users_view',
    },
    {
        title: 'Role',
        iconComponent: 'icon-roles',
        icon: 'SlidersIcon',
        route: 'role-list',
        action: 'read',
        resource: 'roles_view',
    },
    {
        title: 'Company',
        icon: 'BriefcaseIcon',
        route: 'company-list',
        action: 'read',
        resource: 'company_view',
    },
    {
        title: 'All Users',
        iconComponent: 'icon-users',
        icon: '',
        iconImage: 'users.svg',
        route: 'all-users-list',
        action: 'read',
        resource: 'all_users_view',
    },
    {
        title: 'Settings',
        iconComponent: 'icon-settings',
        icon: '',
        iconImage: 'settings.svg',
        route: 'settings',
        action: 'read',
        resource: 'settings_index',
    }
]
