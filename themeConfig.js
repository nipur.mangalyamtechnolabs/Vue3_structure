// Theme Colors
// Initially this will be blank. Later on when app is initialized we will assign bootstrap colors to this from CSS variables.
export const $themeColors = {
  primary: '#c28500'
}
export const $iconColors = {
  primary: "#c28500",
  secondary: "#196400",
  warning: "#ED2224",
  info: "#ED2224",
  success: "#ED2224",
  danger: "#ED2224",
  active: "#FFFFFF",
  mute: "#989898",
}

// App Breakpoints
// Initially this will be blank. Later on when app is initialized we will assign bootstrap breakpoints to this object from CSS variables.
export const $themeBreakpoints = {}

// APP CONFIG
export const $themeConfig = {
  app: {
    appName: 'Skybeat', // Will update name in navigation menu (Branding)
    appVersion: '1.0.1', // Will update name in navigation menu (Branding)
    // eslint-disable-next-line global-require
    appLogoImage: new URL('./src/assets/images/admin/logo.png', import.meta.url).href,
    bgImgOne: new URL('./src/assets/images/admin/Login-BG.png', import.meta.url).href,
    appMonoLogoImage: new URL('./src/assets/images/admin/mono-logo.png', import.meta.url).href, // Will update logo in navigation menu (Branding)
    appNameImage: new URL('./src/assets/images/admin/app-name-logo.png',import.meta.url).href, // Will update logo in navigation menu (Branding)
    appBodyBG: new URL('./src/assets/images/admin/BodyBg.jpg',import.meta.url).href,
    downImg: new URL('./src/assets/images/pages/not-authorized.svg',import.meta.url).href,
    downImgDark: new URL('./src/assets/images/pages/not-authorized-dark.svg',import.meta.url).href,
    successIcon: new URL('./src/assets/images/icons/toast-success.svg',import.meta.url).href,
    alertIcon: new URL('./src/assets/images/icons/toast-alert.svg',import.meta.url).href,
    errorIcon: new URL('./src/assets/images/pages/not-authorized.svg',import.meta.url).href,
    errorSvg: new URL('./src/assets/images/pages/not-authorized.svg',import.meta.url).href,
  },
  layout: {
    isRTL: false,
    skin: 'light', // light, dark, bordered, semi-dark
    routerTransition: 'zoom-fade', // zoom-fade, slide-fade, fade-bottom, fade, zoom-out, none
    type: 'vertical', // vertical, horizontal
    contentWidth: 'full', // full, boxed
    menu: {
      hidden: false,
      isCollapsed: true,
    },
    navbar: {
      // ? For horizontal menu, navbar type will work for navMenu type
      type: 'sticky', // static , sticky , floating, hidden
      backgroundColor: '', // BS color options [primary, success, etc]
    },
    footer: {
      type: 'hidden', // static, sticky, hidden
    },
    customizer: false,
    enableScrollToTop: true,
  },
  user: {
    profilePhoto: ''
  }
}
