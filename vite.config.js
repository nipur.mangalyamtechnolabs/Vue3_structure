import { defineConfig } from 'vite'
import path from 'path';
import vue from '@vitejs/plugin-vue'
import VueDevTools from 'vite-plugin-vue-devtools'
// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue({
      template: {
        compilerOptions: {
          isCustomElement: tag => tag.startsWith('icon-') || tag.startsWith('feather-')
        }
      }
    }),
    VueDevTools(),
  ],
  css: {
    preprocessorOptions: {
      scss: {
        additionalData: `
          
        `,

      }
    }
  },
  resolve: {
    alias: {
      vue: '@vue/compat/dist/vue.esm-bundler.js',
      '@': path.resolve(__dirname, './src'),
      "@core": path.resolve(__dirname, './src/@core'),
      '@themeConfig': path.resolve(__dirname, './themeConfig'),
      '@validations': path.resolve(__dirname, './src/@core/utils/validations/validations.js'),
      '~@resources' : path.resolve(__dirname, './src'),
      '@assets' : path.resolve(__dirname, './src/assets')
    }
  }
})
